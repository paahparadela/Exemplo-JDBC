import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class Main {

	public static void main(String[] args) throws SQLException {
		//Passageiro passageiro = new Passageiro("12345678901", "Lucio Maia", 23, "R Pastor Nascimento", 3, null);
		PassageiroDao passageiroDao = new PassageiroDao();
		//passageiroDao.inserir(passageiro);
		//System.out.println("Cliente inserido com sucesso");
		
		List<Passageiro> passageiros = passageiroDao.consultar();
		
		for(Passageiro passageiro: passageiros) {
			System.out.println(passageiro.getNome()+" "+passageiro.getCpf()+" "+passageiro.getIdade());
		} 
		
	}

}
