import java.math.BigInteger;

public class Passageiro {
	
	private String cpf;
	private String nome;
	private int idade;
	private String rua;
	private int num;
	private String comp;

	public Passageiro() {}
	
	public Passageiro(String cpf, String nome, int idade, String rua, 
			int num, String comp) {
		this.cpf = cpf;
		this.nome = nome;
		this.idade = idade;
		this.rua = rua;
		this.num = num;
		this.comp = comp;
	}
	
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	public String getRua() {
		return rua;
	}
	public void setRua(String rua) {
		this.rua = rua;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public String getComp() {
		return comp;
	}
	public void setComp(String comp) {
		this.comp = comp;
	}
	

}
