import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexao {
	public static Connection getConnection() {
		try {
			return DriverManager.getConnection(
					"jdbc:mysql://localhost/venda_passagem",
					"mastertech",
					"Mastertech@123"
					);
		}catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

}
