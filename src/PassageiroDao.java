import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PassageiroDao {
	private Connection conexao;
	
	public PassageiroDao() {
		this.conexao = Conexao.getConnection();
	}
	
	public void inserir(Passageiro passageiro) {
		String query = "insert into passageiro values "
				+ "(?, ?, ?, ?, ?, ?);";
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			statement.setString(1, passageiro.getCpf());
			statement.setString(2, passageiro.getNome());
			statement.setInt(3, passageiro.getIdade());
			statement.setString(4, passageiro.getRua());
			statement.setInt(5, passageiro.getNum());
			statement.setString(6, passageiro.getComp());
			
			statement.execute();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public List<Passageiro> consultar(){
		String query = "select * from passageiro";
		List<Passageiro> passageiros = new ArrayList<Passageiro>();
		Passageiro passageiro;
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			ResultSet resultado = statement.executeQuery();
			
			while(resultado.next()){
				passageiro = new Passageiro();
				passageiro.setCpf(resultado.getString("cpf"));
				passageiro.setNome(resultado.getString("nome"));
				passageiro.setIdade(resultado.getInt("idade"));
				passageiro.setRua(resultado.getString("rua"));
				passageiro.setNum(resultado.getInt("num"));
				passageiro.setComp(resultado.getString("comp"));
				
				passageiros.add(passageiro);
			}
			resultado.close();
			statement.close();
			
			return passageiros;
		} catch (SQLException e) {
			passageiros = null;
			e.printStackTrace();
			return passageiros;
		}
		//return passageiros;
	}
	
	
	
}
